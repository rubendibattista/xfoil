function fix_makefile () {
    sed -i.bak -E 's=gfortran.*=gfortran-mp-9=g' $1
    sed -i.bak -E 's=gcc.*=gcc-mp-9=g' $1
    sed -i.bak -E 's:finit-real=.*:finit-real=zero:g' $1
    sed -i.bak -E 's:finit-real=.*:finit-real=zero:g' $1
    sed -i.bak -E 's=/usr/X11R6=/opt/local/lib=g' $1
    sed -i.bak -E 's=/usr/X11/include=/opt/local/include=g' $1
    sed -i.bak -E 's:BINDIR =.*:BINDIR = .:g' $1
}

cd orrs
fix_makefile "Makefile"
make clean && make -j 4
cd ..

cd plotlib
cp config.make.gfortranDP config.make
fix_makefile "config.make"
make clean && make -j 4
cd ..

cd bin
fix_makefile "Makefile"
make clean && make -j 4

